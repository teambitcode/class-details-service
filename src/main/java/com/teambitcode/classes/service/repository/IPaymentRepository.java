package com.teambitcode.classes.service.repository;

import com.teambitcode.classes.service.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IPaymentRepository extends JpaRepository<Payment,Integer> {

    @Query("SELECT p from Payment p where p.classGroup.id =:classGroupId ")       // using @query
    List<Payment> findByClassGroupId(@Param("classGroupId") int classGroupId);
}
