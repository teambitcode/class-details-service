package com.teambitcode.classes.service.repository;

import com.teambitcode.classes.service.entity.MainClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClassRepository extends JpaRepository<MainClass, Integer> {
}
