package com.teambitcode.classes.service.repository;

import com.teambitcode.classes.service.entity.ClassGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClassGroupRepository extends JpaRepository<ClassGroup,Integer> {
}
