package com.teambitcode.classes.service.service;

import com.teambitcode.classes.service.entity.MainClass;
import com.teambitcode.classes.service.repository.IClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MainClassService {

    @Autowired
    private IClassRepository classRepository;

    public MainClass save(MainClass mainClass) {
        return classRepository.save(mainClass);
    }
}
