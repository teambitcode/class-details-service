package com.teambitcode.classes.service.service;

import com.teambitcode.classes.service.entity.ClassGroup;
import com.teambitcode.classes.service.repository.IClassGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassGroupService {

    @Autowired
    private IClassGroupRepository classGroupRepository;


    public ClassGroup save(ClassGroup classGroup){
        return classGroupRepository.save(classGroup);
    }
}
