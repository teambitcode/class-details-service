package com.teambitcode.classes.service.service;

import com.teambitcode.classes.service.entity.Payment;
import com.teambitcode.classes.service.repository.IPaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private IPaymentRepository paymentRepository;

    public Payment makePayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    public List<Payment> findByClassGroupId(int classGroupId) {
        return paymentRepository.findByClassGroupId(classGroupId);
    }
}
