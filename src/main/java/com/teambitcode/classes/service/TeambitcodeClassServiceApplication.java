package com.teambitcode.classes.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeambitcodeClassServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeambitcodeClassServiceApplication.class, args);
	}

}
