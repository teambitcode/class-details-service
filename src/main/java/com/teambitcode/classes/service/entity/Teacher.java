package com.teambitcode.classes.service.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
public class Teacher{

    @Id
    @GeneratedValue
    @Column(name = "ID")
    Long id;

    @OneToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    User userID;

    @Column(name = "STATUS")
    String status;

}
