package com.teambitcode.classes.service.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    int id;

    @Column(name = "NAME")
    String name;

    @Column(name = "EMAIL")
    String email;

    @Column(name = "MOBILE_NUMBER")
    String mobileNumber;

    @Column(name = "LOGIN_USER_NAME")
    String userName;

    @Column(name = "LOGIN_PASSWORD")
    String password;

    @Column(name = "GENDER")
    String gender;
}
