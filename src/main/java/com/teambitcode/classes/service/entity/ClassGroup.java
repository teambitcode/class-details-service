package com.teambitcode.classes.service.entity;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@Entity
public class ClassGroup {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    int id;

    @Column(name = "GROUP_NAME")
    String groupName;

    @Column(name = "STATUS")
    String status;
}
