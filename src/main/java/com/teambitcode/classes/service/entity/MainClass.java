package com.teambitcode.classes.service.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class MainClass {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    Long id;

    @Column(name = "NAME")
    String name;

//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mainclass")
//    @JoinColumn(name = "mainclass_id", referencedColumnName = "id")
//    List<Payment> paymentList;

}
