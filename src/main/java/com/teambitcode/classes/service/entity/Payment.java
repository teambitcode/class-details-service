package com.teambitcode.classes.service.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    int id;

    @Column(name = "DATE")
    Date date;

    @Column(name = "AMOUNT")
    Double amount;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "MAIN_CLASS_ID", referencedColumnName = "id")
    MainClass mainClass;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "CLASS_GROUP_ID", referencedColumnName = "id")
    ClassGroup classGroup;
}
