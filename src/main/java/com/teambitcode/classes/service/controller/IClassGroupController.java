package com.teambitcode.classes.service.controller;

import com.teambitcode.classes.service.entity.ClassGroup;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("class-group")
public interface IClassGroupController {

    @Operation(
            summary = "Add class group",
            description = "Add class group",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Save class group response")
            })
    @PostMapping(value = "/save", produces = "application/json")
    ResponseEntity<ClassGroup> saveClassGroup(
            @RequestBody ClassGroup classGroup
    );
}
