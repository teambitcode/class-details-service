package com.teambitcode.classes.service.controller;

import com.teambitcode.classes.service.entity.Student;
import com.teambitcode.classes.service.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController implements IStudentController{

    @Autowired
    StudentService studentService;

    @Override
    public ResponseEntity<Student> saveStudent(Student student) {


        Student student1 = studentService.save(student);
        return  new ResponseEntity<Student>(student1, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Student>> getAllStudents() {
        List<Student> studentList = studentService.getAll();
        return new ResponseEntity<List<Student>>(studentList,HttpStatus.OK);
    }
}
