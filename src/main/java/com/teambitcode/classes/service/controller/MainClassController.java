package com.teambitcode.classes.service.controller;

import com.teambitcode.classes.service.entity.MainClass;
import com.teambitcode.classes.service.service.MainClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainClassController implements IMainClassController {

    @Autowired
    private MainClassService mainClassService;

    @Override
    public ResponseEntity<MainClass> saveMainClass(MainClass mainClass) {
        MainClass save = mainClassService.save(mainClass);
        return new ResponseEntity<MainClass>(save, HttpStatus.OK);
    }
}
