package com.teambitcode.classes.service.controller;

import com.teambitcode.classes.service.config.MqConfig;
import com.teambitcode.classes.service.config.RabbitMqSingletonConfig;
import com.teambitcode.classes.service.entity.ClassGroup;
import com.teambitcode.classes.service.entity.Payment;
import com.teambitcode.classes.service.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PaymentController implements IPaymentController {

    @Autowired
    private PaymentService paymentService;

    @Override
    public ResponseEntity<Payment> makePayment(Payment payment) {

//        Payment payment1 = paymentService.makePayment(payment);
        MqConfig mqConfig = new MqConfig();
        mqConfig.setPort(5672);
        mqConfig.setQueueName("first_message");
        mqConfig.setHostAddress("localhost");
        mqConfig.setUserName("guest");
        mqConfig.setPassword("guest");

        RabbitMqSingletonConfig.getInstance().publishMessage(mqConfig,"first message","first_message");
        return new ResponseEntity<Payment>(new Payment(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Payment>> getAllPaymentsByClassGroup(int classGroupId) {
        List<Payment> paymentList = paymentService.findByClassGroupId(classGroupId);

        return new ResponseEntity<List<Payment>>(paymentList, HttpStatus.OK);
    }
}
