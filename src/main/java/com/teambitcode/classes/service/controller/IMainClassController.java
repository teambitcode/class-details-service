package com.teambitcode.classes.service.controller;

import com.teambitcode.classes.service.entity.MainClass;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("main-class")
public interface IMainClassController {

    @Operation(
            summary = "Add main class",
            description = "Add main class",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Save main class response")
            })
    @PostMapping(value = "/save", produces = "application/json")
    ResponseEntity<MainClass> saveMainClass(
            @RequestBody MainClass mainClass
    );
}
