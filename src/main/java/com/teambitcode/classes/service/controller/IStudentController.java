package com.teambitcode.classes.service.controller;

import com.teambitcode.classes.service.entity.Student;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("student")
public interface IStudentController {

    @Operation(
            summary = "Add Products to Cart",
            description = "Add Products to Cart",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description= "Save a student response" )
            } )
    @PostMapping( value = "/save", produces = "application/json" )
    ResponseEntity<Student> saveStudent( @RequestBody Student student);


    @GetMapping(value = "")
    ResponseEntity<List<Student>> getAllStudents();
}
