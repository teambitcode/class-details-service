package com.teambitcode.classes.service.controller;

import com.teambitcode.classes.service.entity.ClassGroup;
import com.teambitcode.classes.service.service.ClassGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClassGroupController implements IClassGroupController{

    @Autowired
    private ClassGroupService classGroupService;

    @Override
    public ResponseEntity<ClassGroup> saveClassGroup(ClassGroup classGroup) {
        ClassGroup classGroup1 = classGroupService.save(classGroup);

        return new ResponseEntity<ClassGroup>(classGroup1, HttpStatus.OK);
    }
}
