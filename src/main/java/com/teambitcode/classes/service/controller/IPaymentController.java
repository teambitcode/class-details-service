package com.teambitcode.classes.service.controller;

import com.teambitcode.classes.service.entity.ClassGroup;
import com.teambitcode.classes.service.entity.Payment;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("payment")
public interface IPaymentController {

    @Operation(
            summary = "Make payment",
            description = "Make payment",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Make payment response")
            })
    @PostMapping(value = "/save", produces = "application/json")
    ResponseEntity<Payment> makePayment(@RequestBody Payment payment);


    @Operation(
            summary = "Add class group",
            description = "Add class group",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Save class group response")
            })
    @GetMapping(value = "/{classGroupId}", produces = "application/json")
    ResponseEntity<List<Payment>> getAllPaymentsByClassGroup(
            @PathVariable("classGroupId") int classGroupId
    );
}
