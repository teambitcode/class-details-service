package com.teambitcode.classes.service.config;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import com.rabbitmq.client.Channel;
import org.springframework.context.annotation.Configuration;

public class RabbitMqSingletonConfig {
    private Connection mqConnection;
    private static final RabbitMqSingletonConfig INSTANCE = new RabbitMqSingletonConfig();
    private static CachingConnectionFactory factory = null;

    public static RabbitMqSingletonConfig getInstance(){
        return INSTANCE;
    }

    private Connection getConnection(MqConfig mqConfig) {
        try {
            factory = new CachingConnectionFactory(mqConfig.getHostAddress());

            // ConnectionFactory factory = new ConnectionFactory();

            factory.setUsername(mqConfig.getUserName());
            factory.setPassword(mqConfig.getPassword());
            factory.setPort(mqConfig.getPort());
            factory.setHost(mqConfig.getHostAddress());
            factory.setConnectionTimeout(300000);
            factory.setConnectionLimit(3);


            // getting a connection
            if (mqConnection == null || !mqConnection.isOpen()) {
                if (mqConnection != null) {
//                    logger.debug("Existing Connection status : " + mqConnection.isOpen());
                }
                mqConnection = factory.createConnection();
//                logger.debug("New Connection status : " + connection.isOpen());
            }
        } catch (Exception ex) {
//            logger.error("Create Connection Error : " + ex.toString());
            ex.printStackTrace();
            connectionClose();
        }
        if (mqConnection != null) {
//            logger.debug("Connection established");

        }
        return mqConnection;
    }

    private void connectionClose() {
        try {
            if (mqConnection.isOpen()) {
//                logger.debug("Connection Open, So closing");
                mqConnection.close();
                mqConnection = null;
            }
        } catch (Exception e) {

//            logger.debug("Error closing connection " + e.toString());
            mqConnection = null;
        }
    }

    public void publishMessage(MqConfig mqConfig, Object jsonObj, String routingKey) {
        try {

            // creating a channel

            mqConnection = getConnection(mqConfig);
            if (mqConnection != null) {
                Channel channel = mqConnection.createChannel(false);

                if (channel != null) {
                    channel.exchangeDeclare("my.direct","direct");
                    channel.queueDeclare(mqConfig.getQueueName(),false,false,false,null);
                    channel.queueBind(mqConfig.getQueueName(), "my.direct", routingKey);
                    channel.basicPublish("my.direct", routingKey, null,
                            jsonObj.toString().getBytes("utf-8"));
                    channel.close();
                }

            }
        } catch (Exception e) {
System.out.println(e);
            e.printStackTrace();
            //            logger.error("Error connecting to Queue : " + e.toString());
//            logger.error(e.getMessage());
            connectionClose();
        }
    }
}
