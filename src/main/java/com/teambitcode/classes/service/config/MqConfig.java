package com.teambitcode.classes.service.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MqConfig {
    String queueName;
    String hostAddress;
    String userName;
    String password;
    int port;
}
