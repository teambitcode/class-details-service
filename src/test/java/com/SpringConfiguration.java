package com;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
        basePackages = {"com.teambitcode.classes.service.controller",
                "com.teambitcode.classes.service.entity",
                "com.teambitcode.classes.service.repository",
                "com.teambitcode.classes.service.service"})
public class SpringConfiguration {

}