package com.teambitcode.classes.service.test.repository;

import com.teambitcode.classes.service.entity.Student;
import com.teambitcode.classes.service.repository.IStudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class StudentRepositoryTest {

    @Autowired
    private IStudentRepository studentRepository;

    @Test
    public void testCreateCountry(){
        Student student = new Student(10,"12345555", null);
        assertNotNull(student);
    }
}