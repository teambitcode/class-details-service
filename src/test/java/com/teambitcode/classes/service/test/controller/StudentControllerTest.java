package com.teambitcode.classes.service.test.controller;

import com.teambitcode.classes.service.controller.StudentController;
import com.teambitcode.classes.service.entity.Student;
import com.teambitcode.classes.service.entity.User;
import com.teambitcode.classes.service.service.StudentService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@TestInstance( TestInstance.Lifecycle.PER_CLASS )
//@SpringBootTest
public class StudentControllerTest {

    @InjectMocks
    private StudentController studentController;

    @Mock
    private StudentService mockStudentService;

    @MockBean
    private MockMvc mockMvc;

    @BeforeAll
    void setup(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc =  MockMvcBuilders.standaloneSetup(studentController).build();
    }

//    @Test
//    void contextLoads(ApplicationContext context) {
//        assertThat(context).isNotNull();
//    }

    @Test
    public void testStudentGetAllRequest(){
        System.out.println();


        List<Student> studentList = new ArrayList<>();

        User user = new User();
        studentList.add(new Student(1,"12345671",user));
        studentList.add(new Student(2,"12345672",user));
        studentList.add(new Student(3,"12345673",user));
        studentList.add(new Student(4,"12345674",user));
        studentList.add(new Student(5,"12345675",user));
        studentList.add(new Student(6,"12345676",user));


        Mockito.when(mockStudentService.getAll()).thenReturn(studentList);

        try {
            mockMvc.perform(MockMvcRequestBuilders.get("/student")).andExpect(
                    MockMvcResultMatchers.jsonPath("$[0].nic", Matchers.is("12345671"))
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
